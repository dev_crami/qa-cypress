/// <reference types="cypress-xpath"></reference>

// Correct Login Access Test
describe('Login Access Test', () => {

    // Tests to be executed...
    it('Fill Credentials and Click Login', () => {       

        // URL Load
        cy.visit('https://opensource-demo.orangehrmlive.com/');

        // txtUsername
        cy.get('#txtUsername').type("Admin")

        // txtPassword
        cy.get('#txtPassword').type("admin123")

        // btnLogin
        cy.url().then(url => {
            cy.get('#btnLogin').click();
            cy.url().should('eq','https://opensource-demo.orangehrmlive.com/index.php/dashboard');
        });        
    });
    }
);

// User Forgot Password Test
describe('User Forgot Password Test', () => {

    // Aquí dentro van todas las pruebas que se deben ejecutar
    it('Rellenar el formulario', () => {
                
        // Navegamos a la raíz de la web
        cy.visit('https://opensource-demo.orangehrmlive.com/');


        cy.url().then(url => {
            cy.get('#forgotPasswordLink > a').click();
            
            cy.url().should('eq','https://opensource-demo.orangehrmlive.com/index.php/auth/requestPasswordResetCode');
        });

        cy.get('#securityAuthentication_userName').type("Admin")
        cy.get('#btnSearchValues').click()

        cy.get('.message').should('be.visible')

    });
    }
);