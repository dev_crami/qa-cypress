/// <reference types="cypress-xpath"></reference>

/** Usernames,Passwords & messages must be inserted carefully and in order */
const usernames = ["","Admin","","Wronguser"]
const passwords = ["","","admin123","wrongpass"]
const messages = ["Username cannot be empty","Password cannot be empty","Username cannot be empty","Invalid credentials"]

// Incorrect Login Access Test
describe('Incorrect Login Access Test', () => {

    it('Incorrect login with User,Password and Message expected', () => {
                
        for (var i = 0; i < usernames.length; i++) {
            console.log(usernames[i]);
            console.log(passwords[i]);
            
            cy.visit('https://opensource-demo.orangehrmlive.com/');

            // txtUsername
            if (usernames[i].toLowerCase().localeCompare("")){
                cy.get('#txtUsername').type(usernames[i])
            }          

            // txtPassword 
            if (passwords[i].toLowerCase().localeCompare("")){
                cy.get('#txtPassword').type(passwords[i])
            }            

            // btnLogin
            cy.get('#btnLogin').click();
            cy.get('#spanMessage').should('be.visible')
            cy.contains(messages[i])
        }
    });
    }
);